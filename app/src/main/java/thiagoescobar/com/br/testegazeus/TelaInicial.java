package thiagoescobar.com.br.testegazeus;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class TelaInicial extends AppCompatActivity {

    public static int dificuldade = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);

        final RadioButton rbDif1 = (RadioButton) findViewById(R.id.rbFacil);
        final RadioButton rbDif2 = (RadioButton) findViewById(R.id.rbMedio);
        final RadioButton rbDif3 = (RadioButton) findViewById(R.id.rbDificil);

        rbDif1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dificuldade = 1;
                rbDif2.setChecked(false);
                rbDif3.setChecked(false);
            }
        });

        rbDif2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dificuldade = 2;
                rbDif1.setChecked(false);
                rbDif3.setChecked(false);
            }
        });

        rbDif3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dificuldade = 3;
                rbDif2.setChecked(false);
                rbDif1.setChecked(false);
            }
        });


        Button btPong = (Button) findViewById(R.id.btPong);
        btPong.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(dificuldade != 0) {
                    MediaPlayer mp = MediaPlayer.create(TelaInicial.this, R.raw.iniciodojogo);
                    mp.start();
                    Intent intentPong = new Intent(TelaInicial.this, Pong.class);
                    startActivity(intentPong);
                }else{
                    Toast toast = Toast.makeText(TelaInicial.this, "Por favor selecione uma dificuldade", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }
}
