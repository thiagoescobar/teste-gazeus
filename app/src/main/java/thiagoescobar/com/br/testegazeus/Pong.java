package thiagoescobar.com.br.testegazeus;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class Pong extends AppCompatActivity {

    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pong);
    }
    public void fimDeJogo(){
        Intent intentPong = new Intent(Pong.this,TelaInicial.class);
        intentPong.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentPong);
    }
    public void playSound(int musica){
        mp = MediaPlayer.create(Pong.this, musica);
        mp.start();
    }
}