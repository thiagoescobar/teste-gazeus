package thiagoescobar.com.br.testegazeus.pong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.view.SurfaceHolder;

import thiagoescobar.com.br.testegazeus.R;

/**
 * Created by Thiago Escobar on 24/06/2017.
 */

public class PongThread extends Thread {
    /** Handle to the surface manager object we interact with */
    private SurfaceHolder _surfaceHolder;
    private Paint _paint;
    private PongState _state;
    private Boolean _jogoRolando = true;
    private PongView _pongView;

    public PongThread(SurfaceHolder surfaceHolder, Context context, Handler handler, PongView pongView)
    {
        _surfaceHolder = surfaceHolder;
        _paint = new Paint();
        _state = new PongState(context, this);
        _pongView = pongView;
    }

    @Override
    public void run() {
        while(_jogoRolando)
        {
            Canvas canvas = _surfaceHolder.lockCanvas();
            _state.update();
            _state.draw(canvas,_paint);
            _surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    public PongState getGameState()
    {
        return _state;
    }

    public void click(Boolean apertou, int coordX){
        _state.click(apertou, coordX);
    }
    public void fimDeJogo(){
        _jogoRolando = false;
        _pongView.fimDeJogo();
    }
    public void playSound(int musica){
        _pongView.playSound(musica);
    }
}