package thiagoescobar.com.br.testegazeus.pong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.view.Display;
import android.view.KeyEvent;
import android.view.WindowManager;

import thiagoescobar.com.br.testegazeus.Pong;
import thiagoescobar.com.br.testegazeus.R;
import thiagoescobar.com.br.testegazeus.TelaInicial;

/**
 * Created by Germinar 2 on 24/06/2017.
 */

public class PongState {

    //screen width and height
    int _screenWidth;
    int _screenHeight;

    //The ball
    int _ballSize;
    int _ballX;
    int _ballY;
    int _ballVelocityX;
    int _ballVelocityY;
    int _velocidadeBaseDaBola;

    //The bats
    int _batLength;
    int _batHeight;
    int _topBatX;
    int _topBatY;
    int _bottomBatX;
    int _bottomBatY;
    int _batSpeed;

    int movUsuario = 0;
    boolean _desenhaBola = true;
    int alvoX;

    int pontoCPU;
    int pontoJogador;

    int contadorPonto;
    int contadorPartida;
    int contadorFimDeJogo;
    boolean fimDeJogo = false;

    PongThread _pongThread;

    public PongState(Context context, PongThread pongThread)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        _screenWidth = size.x;
        _screenHeight = size.y;
        _ballSize = _screenWidth/30;
        _ballX = _screenWidth/2;
        _ballY = _screenHeight/5;
        _batLength = _screenWidth/5;
        _batHeight = _batLength/10;
        _topBatX = (_screenWidth/2) - (_batLength / 2);
        _topBatY = _screenHeight/10;
        _bottomBatX = (_screenWidth/2) - (_batLength / 2);
        _bottomBatY = _screenHeight - (_screenHeight/5);
        _batSpeed = 5+(TelaInicial.dificuldade*2);
        _pongThread = pongThread;
        pontoCPU = 0;
        pontoJogador = 0;
        contadorPonto = 0;
        alvoX = _bottomBatX;
        _velocidadeBaseDaBola = 3+ (TelaInicial.dificuldade*2);
        _ballVelocityX = _velocidadeBaseDaBola;
        _ballVelocityY = _velocidadeBaseDaBola;
        contadorPartida = 0;
        contadorFimDeJogo = 0;
    }

    //The update method
    public void update() {
        if(_bottomBatX+(_batLength / 2 ) != alvoX){
            if(_bottomBatX+(_batLength / 2 )>alvoX) {
                if((_bottomBatX+(_batLength / 2 )-alvoX)<movUsuario){
                    _bottomBatX = alvoX-(_batLength / 2 );
                }else {
                    _bottomBatX -= movUsuario;
                }
                if(_bottomBatX<0){
                    _bottomBatX = 0;
                }
            }else{
                if(alvoX-(_bottomBatX+(_batLength / 2 ))<movUsuario){
                    _bottomBatX = alvoX-(_batLength / 2 );
                }else{
                    _bottomBatX += movUsuario;
                }
                if(_bottomBatX+_batLength > _screenWidth){
                    _bottomBatX = _screenWidth-_batLength;
                }
            }
        }
        int alvoInimigo = _ballX+(_ballSize/2);
        if(alvoInimigo >( _topBatX +(_batLength/2))) {
            if((_topBatX+(_batLength / 2 )-alvoInimigo)<_batSpeed){
                _topBatX = alvoInimigo-(_batLength/2);
            }else {
                _topBatX += _batSpeed;
            }
            if(_topBatX +_batLength >_screenWidth){
                _topBatX = _screenWidth -_batLength;
            }
        }else{
            if(alvoInimigo-(_topBatX+(_batLength / 2 ))<_batSpeed) {
                _topBatX = alvoInimigo-(_batLength/2);
            }else {
                _topBatX -= _batSpeed;
            }
            if(_topBatX<0) {
                _topBatX = 0;
            }
        }

        _ballX += _ballVelocityX;
        _ballY += _ballVelocityY;

        if(contadorPonto == 0) {
            //Ponto
            if (_ballY > _bottomBatY+_velocidadeBaseDaBola || _ballY < _topBatY-_velocidadeBaseDaBola) {
                _desenhaBola = false;
                if (_ballY > _bottomBatY+_velocidadeBaseDaBola ) {
                    pontoCPU ++;
                } else {
                    pontoJogador ++;
                }
                if(pontoCPU == 2 || pontoJogador ==2){
                    fimDeJogo = true;
                    contadorFimDeJogo = 60;
                }
                _ballX = _screenWidth/2;
                _ballY = _screenHeight/2;
                contadorPonto = 60;
                _pongThread.playSound(R.raw.ponto);
            }else{
                _desenhaBola = true;
            }

            //Colisão com os lados
            if ((_ballX + _ballSize) > _screenWidth || _ballX < 0) {
                if(_ballVelocityX>0){
                    _ballX = _screenWidth - _ballSize;
                    _ballVelocityX = _velocidadeBaseDaBola*-1;
                }else {
                    _ballX = 0;
                    _ballVelocityX = _velocidadeBaseDaBola;
                }
                _pongThread.playSound(R.raw.batida_lateral);
            }

            //Colisão com o inimigo
            if (_ballX > _topBatX && _ballX < _topBatX + _batLength && (_ballY < _topBatY + _batHeight + (_velocidadeBaseDaBola))) {
                if(_ballVelocityY>0){
                    _ballVelocityY = _velocidadeBaseDaBola*-1;
                }else {
                    _ballVelocityY = _velocidadeBaseDaBola;
                }
                _ballY = _topBatY + _batHeight + _velocidadeBaseDaBola+1;
                _pongThread.playSound(R.raw.rebatida);
            }
            //Colisão com o jogador
            if (_ballX > _bottomBatX && _ballX < _bottomBatX + _batLength && ((_ballY+_ballSize) >= _bottomBatY)) {
                if(_ballVelocityY>0){
                    _ballVelocityY = _velocidadeBaseDaBola*-1;
                }else {
                    _ballVelocityY = _velocidadeBaseDaBola;
                }
                _ballY = _bottomBatY - _ballSize -1;
                _pongThread.playSound(R.raw.rebatida);
            }
            contadorPartida ++;
            _velocidadeBaseDaBola = (3+(TelaInicial.dificuldade*2))+(Math.round(contadorPartida/(400-TelaInicial.dificuldade*100)));
        }else{
            if(!fimDeJogo) {
                contadorPonto--;
                contadorPartida = 0;
                _ballX = _screenWidth / 2;
                _ballY = _screenHeight / 2;
                _topBatX = (_screenWidth / 2) - (_batLength / 2);
                _bottomBatX = (_screenWidth / 2) - (_batLength / 2);
                _velocidadeBaseDaBola = 5;
                _ballVelocityX = _velocidadeBaseDaBola;
                _ballVelocityX = _velocidadeBaseDaBola;
            }
        }
    }

    public void click(Boolean apertou, int coordX){
        if(apertou){
            alvoX = coordX;
            movUsuario = 12;
        }else{
            //significa que o usuário soltou o botão
            movUsuario = 0;
        }
    }
    //the draw method
    public void draw(Canvas canvas, Paint paint) {
        //desenha os pontos
        if (contadorPonto == 0) {
            //Clear the screen
            canvas.drawRGB(20, 20, 20);

            //set the colour
            paint.setARGB(200, 0, 200, 0);

            //draw the ball
            if (_desenhaBola) {
                canvas.drawRect(new Rect(_ballX, _ballY, _ballX + _ballSize, _ballY + _ballSize),
                        paint);

            //draw the bats
                canvas.drawRect(new Rect(_topBatX, _topBatY, _topBatX + _batLength,
                        _topBatY + _batHeight), paint); //top bat
                canvas.drawRect(new Rect(_bottomBatX, _bottomBatY, _bottomBatX + _batLength,
                        _bottomBatY + _batHeight), paint); //bottom bat
            }
           //canvas.drawRect(new Rect(0, _topBatY-20, _screenWidth, 0), paint);
            paint.setARGB(100, 0, 200, 0);
            canvas.drawRect(new Rect(0, _bottomBatY+20+_batHeight, _screenWidth, _screenHeight), paint);
            canvas.drawRect(new Rect(0, 0, _screenWidth, _topBatY-20), paint);
            paint.setARGB(100, 0, 200, 0);
            paint.setTextSize(46);
            canvas.drawText("Jogador: " + pontoJogador + " CPU:" + pontoCPU, (_screenWidth /2)-180, (_screenHeight /2)-30, paint);
        }else{
            if(fimDeJogo){
                if(contadorFimDeJogo>0) {
                    canvas.drawRGB(20, 20, 20);
                    paint.setARGB(200, 0, 200, 0);
                    paint.setTextSize(100);
                    String nomeGanhador;
                    if (pontoCPU > 4) {
                        nomeGanhador = "Computador";
                    } else {
                        nomeGanhador = "Jogador";
                    }
                    canvas.drawText("Vitória do " + nomeGanhador, (_screenWidth / 5), (_screenHeight / 2), paint);
                    contadorFimDeJogo--;
                }else{
                    _pongThread.fimDeJogo();
                }
            }else {
                canvas.drawRGB(20, 20, 20);
                paint.setARGB(200, 0, 200, 0);
                paint.setTextSize(150);
                canvas.drawText("PONTO!", (_screenWidth / 2) - 300, (_screenHeight / 2), paint);
            }

        }
    }

}
