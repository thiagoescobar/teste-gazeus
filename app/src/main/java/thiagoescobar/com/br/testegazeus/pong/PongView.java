package thiagoescobar.com.br.testegazeus.pong;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.widget.Toast;

import thiagoescobar.com.br.testegazeus.Pong;
import thiagoescobar.com.br.testegazeus.R;
import thiagoescobar.com.br.testegazeus.TelaInicial;

/**
 * Created by Thiago Escobar on 24/06/2017.
 */

public class PongView extends SurfaceView implements SurfaceHolder.Callback {

    private PongThread _thread;
    private Context contexto;

    public PongView(Context context, AttributeSet attrs) {
        super(context, attrs);
        contexto = context;
        //So we can listen for events...
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setFocusable(true);

        //and instantiate the thread
        _thread = new PongThread(holder, context, new Handler(), this);
    }

    public boolean onTouchEvent(MotionEvent event){
        switch(event.getActionMasked()) {
            case MotionEvent.ACTION_UP:
                _thread.click(false, (int)event.getX());
                break;
            case MotionEvent.ACTION_MOVE:
                _thread.click(true, (int)event.getX());
                break;
            case MotionEvent.ACTION_DOWN:
                _thread.click(true, (int)event.getX());
                break;
            default:
                break;
        }
        return true;
    }

    //Implemented as part of the SurfaceHolder.Callback interface
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        //Mandatory, just swallowing it for this example
    }
    //Implemented as part of the SurfaceHolder.Callback interface
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        _thread.start();
    }
    //Implemented as part of the SurfaceHolder.Callback interface
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        _thread.interrupted();
    }
    public void fimDeJogo(){
        Pong activity = (Pong)contexto;
        activity.fimDeJogo();
    }
    public void playSound(int musica) {
        Pong activity = (Pong)contexto;
        activity.playSound(musica);
    }
}
